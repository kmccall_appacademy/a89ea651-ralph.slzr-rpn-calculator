class RPNCalculator
  OPERATORS = ['+', '-', '*', '/']
  attr_accessor :num_input, :value

  def initialize(num_input)
    @num_input = num_input
  end

  def calculator(num_input)
    input_array = num_input.split(' ')
    num_stack = []

    input_array.each do |el|
      if OPERATORS.include?(el)
        num_pop_one = num_stack.pop #last number in stack
        num_pop_two = num_stack.pop #second to last number in stack
        current_calc = num_pop_two.method(el).(num_pop_one)
        num_stack << current_calc #pushes the current resolution to the stack
      else
        num_stack << el.to_i
      end
    end
    @value = num_stack[0]
  end
end
